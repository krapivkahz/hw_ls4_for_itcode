import telebot
from telebot.types import Message
import requests

from TOKEN import TOKEN


bot = telebot.TeleBot(token=TOKEN)


@bot.message_handler(content_types=["text"])
def echo(message: Message):
    response = requests.get("https://random.dog/woof.json").json()
    url = response['url'].split(".")[-1]
    if url == "gif" or "mp4":
        bot.send_animation(message.chat.id, response['url'])
    elif url == "jpeg" or "jpg" or "png" or "JPG":
        bot.send_photo(message.chat.id, response['url'])


bot.polling()